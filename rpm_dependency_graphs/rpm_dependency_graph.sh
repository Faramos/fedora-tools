#!/bin/bash

# ========================
#
# Author: Michal Schorm <mschorm@centrum.cz>
# Creation date: 10/2020
#
# Tested on: Fedora 32; Bash 5
#
# ========================

# HELP
HELP="
DESCRIPTION:
  This script produce SVG graph of the RPM package dependencies of the specified package (family).

DETAILS:
  First a Mock chroot is created and populated.
  Than the package is installation is resolved by DNF, but the packages are downloaded instead of installed.
  A repository consisting only from the downloaded packages is created.
  DNF repograph is ran on the repository.
  The graph is visualized in SVG and copied out of the chroot to your current workdir.

ARGUMENTS:
  -p | --package        Package to draw graph of (e.g. 'mariadb')

  -i | --install        Aditional packages to install to the chroot - such won't be covered in the graph (e.g. 'perl-interpreter')
  -o | --output         Output file name. '.svg' string will be appended. (e.g. 'resulting_graph')
  -c | --chroot         Mock chroot to use (e.g. 'fedora-32-x86_64')

  -h | --help           Prints help

EXAMPLES:
  $0 -c fedora-32-x86_64 -p mariadb -i perl-interpreter
  $0 -c fedora-32-x86_64 -p community-mysql -i perl-interpreter -o mysql
"

# ====================

# CMD line arguments processing
POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -p|--package)
    PACKAGE="$2"
    shift # past argument
    shift # past value
    ;;
    -c|--chroot)
    CHROOT="$2"
    shift # past argument
    shift # past value
    ;;
    -o|--output)
    OUTPUT="$2"
    shift # past argument
    shift # past value
    ;;
    -i|--install)
    ADITIONAL_PACKAGES="$2"
    shift # past argument
    shift # past value
    ;;
    -h|--help)
    echo -e "$HELP"; exit 0;
    shift # past argument
    shift # past value
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

# Input validation
if [ -z "$CHROOT" ];
then
  echo "The chroot must be specified"
  exit 1
fi

if [ ! -e "/etc/mock/$CHROOT.cfg" ];
then
  echo "The selected chroot is not available as your mock configuration (in /etc/mock)"
  exit 1
fi

if [ -z "$PACKAGE" ];
then
  echo "The name of the package can't be empty"
  exit 1
fi

if [ -z "$OUTPUT" ];
then
  echo "The name of the output file can't be empty. Using the package name instead."
  OUTPUT=$PACKAGE
fi

# ====================

echo -e "
Configured parameters:
\tPackage:          $PACKAGE
\tChroot:           $CHROOT
\tOutput file name: $OUTPUT.svg

\tAditional packages to install to the chroot: $ADITIONAL_PACKAGES
"

# ====================

# The script itself
mock -r "$CHROOT" --init
mock -r "$CHROOT" --install dnf createrepo_c dnf-utils graphviz @core
if [ -n "$ADITIONAL_PACKAGES" ]; then mock -r "$CHROOT" --install "$ADITIONAL_PACKAGES"; fi

mock -r "$CHROOT" --enable-network --chroot \
"cd /root && mkdir -p REPO && cd REPO && dnf install -y --skip-broken --downloadonly --downloaddir=./ \"$PACKAGE*\" --exclude=\"*devel*\" --exclude=\"*test*\" && createrepo_c ./ "

mock -r "$CHROOT" --chroot \
"echo -e \"[graph]\nname=graph\nbaseurl=file:///root/REPO\" > /root/graph.repo && dnf -q repograph --repo=graph --setopt=reposdir=/root > /root/repo.dot"

mock -r "$CHROOT" --chroot \
"dot -Tsvg -Nstyle=bold -Nshape=box -Estyle=bold -o /tmp/graph.svg /root/repo.dot && ls -1 /root"

mock -r "$CHROOT" --copyout /tmp/graph.svg ./"$OUTPUT.svg"

# ====================

# TODO: aditional packages to install
# TODO: aditional packages to blacklist
# TODO: argumets can be specified multiple times
